import sys

sys.path.append('/usr/share/pywps')

from pywps.app.Service import Service

# processes need to be installed in PYTHON_PATH
from processes.sayhello import SayHello

processes = [
    SayHello()
]

# Service accepts two parameters:
# 1 - list of process instances
# 2 - list of configuration files
application = Service(
    processes,
    ['/etc/pywps/pywps-wsgi.cfg']
)
